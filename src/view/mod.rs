use crate::entity;
use crate::presenter;

pub struct Cli {}

impl Cli {
    pub fn new() -> Cli {
        Cli {}
    }

    fn display(&self, vm: entity::ViewModel) {
        vm.elements.iter().for_each(|e| println!("{}", e));
    }
}

impl presenter::PresenterOutPort for Cli {
    fn present(&self, vm: entity::ViewModel) {
        self.display(vm);
    }
}

// -----------------------------------------------------------------------------
