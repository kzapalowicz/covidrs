use crate::entity;
use crate::usecase;

pub trait PresenterOutPort {
    fn present(&self, response: entity::ViewModel);
}

pub struct Table {
    output: Box<dyn PresenterOutPort>,
}

impl Table {
    pub fn with_output(out: Box<dyn PresenterOutPort>) -> Table {
        Table { output: out }
    }
}

impl usecase::LatestCountryDataOutPort for Table {
    fn response(&self, response: entity::LatestDataResponse) {
        let mut v: Vec<String> = Vec::new();

        v.push(format!(
            "+---------------+-------------+------------+-----------+"
        ));
        v.push(format!(
            "| Country       | Total Cases | Deaths     | New Cases |"
        ));
        v.push(format!(
            "+---------------+-------------+------------+-----------+"
        ));
        response.data.iter().for_each(|d| v.push(format!("{}", d)));
        v.push(format!(
            "+---------------+-------------+------------+-----------+"
        ));

        self.output.present(entity::ViewModel { elements: v });
    }
}
