use crate::entity;

const CORONA_URL: &str = "https://corona.lmao.ninja/countries/";

pub trait LatestCountryDataInPort {
    fn get_data(&self, request: entity::LatestDataRequest);
}

pub trait LatestCountryDataOutPort {
    fn response(&self, response: entity::LatestDataResponse);
}

pub struct LatestCountryData {
    output: Box<dyn LatestCountryDataOutPort>,
}

impl LatestCountryData {
    pub fn with_output(out: Box<dyn LatestCountryDataOutPort>) -> LatestCountryData {
        LatestCountryData { output: out }
    }

    fn get(&self, country: &str) -> Option<entity::Country> {
        match reqwest::blocking::get(format!("{}{}", CORONA_URL, country).as_str()) {
            Ok(resp) => match resp.json::<entity::Country>() {
                Ok(e) => Some(e),
                Err(_) => None,
            },
            Err(_) => None,
        }
    }
}

impl LatestCountryDataInPort for LatestCountryData {
    fn get_data(&self, request: entity::LatestDataRequest) {
        let v: Vec<entity::Country> = request
            .countries
            .iter()
            .filter_map(|c| self.get(c.as_str()))
            .collect();
        if v.len() > 0 {
            self.output.response(entity::LatestDataResponse { data: v });
        }
    }
}
