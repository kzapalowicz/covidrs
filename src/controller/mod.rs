use crate::entity;
use crate::usecase;

pub struct Controller<'a> {
    usecase: &'a dyn usecase::LatestCountryDataInPort,
}

impl Controller<'_> {
    pub fn with_usecase(uc: &dyn usecase::LatestCountryDataInPort) -> Controller {
        Controller { usecase: uc }
    }

    pub fn request(&self, a: std::env::Args) {
        self.usecase.get_data(entity::LatestDataRequest::from(a));
    }
}
