mod controller;
mod entity;
mod presenter;
mod usecase;
mod view;

use std::env;

fn main() {
    let view = Box::new(view::Cli::new());
    let presenter = Box::new(presenter::Table::with_output(view));
    let uc = usecase::LatestCountryData::with_output(presenter);

    let ctrl = controller::Controller::with_usecase(&uc);
    ctrl.request(env::args());
}
