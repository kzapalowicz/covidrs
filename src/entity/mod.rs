use serde::{Deserialize, Serialize};
use std::fmt;

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize, Debug)]
pub struct Country {
    active: u32,
    cases: u32,
    country: String,
    critical: u32,
    deaths: u32,
    recovered: u32,
    todayCases: u32,
    todayDeaths: u32,
    updated: u64,
}

impl fmt::Display for Country {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "| {:13} | {:11} | {:10} | {:9} |",
            self.country, self.cases, self.deaths, self.todayCases
        )
    }
}

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize, Debug)]
pub struct LatestDataRequest {
    pub worldwide: bool,
    pub countries: Vec<String>,
    pub states: bool,
}

impl From<std::env::Args> for LatestDataRequest {
    fn from(args: std::env::Args) -> Self {
        // 1st element is the name of this program as usual on args. need to
        // skip it.
        let v = args.skip(1).collect();
        LatestDataRequest {
            worldwide: false,
            countries: v,
            states: false,
        }
    }
}

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize, Debug)]
pub struct LatestDataResponse {
    pub data: Vec<Country>,
}

pub struct ViewModel {
    pub elements: Vec<String>,
}
