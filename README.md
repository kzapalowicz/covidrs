# covidrs

The Rust commandline client to the Covid-19 data provided by the
[NovelCOVID API](https://corona.lmao.ninja). It is inspired by
[ahmadawais/corona-cli](https://github.com/ahmadawais/corona-cli).

It has been created to allow for tracking the current corona virus situation on
the world and also to excersise the [Clean Architecture](https://https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
design in Rust.

License: MIT

# Installation

Currently, the only way to install *covidrs* is to clone the repository and
build it from the source. Note that for this to succedd one needs to have the
[Rust](https://rust-lang.org) installed on the system. Follow
[Install Rust](https://www.rust-lang.org/tools/install) instructions on how to
do it. Once it is completed clone this repository, enter to the top-level
directory and build it as any Rust program.

```bash
$ git clone https://gitlab.com/kzapalowicz/covidrs
$ cd covidrs
$ cargo build
```

The binary will be avalable as `target/debug/covidrs`

# Usage

It allows for fetching the latest data for each country specified. The
commandline args are the input to the application.

```bash
$ ./target/debug/covidrs Italy Germany USA
```

will provide results for these three countries - you get the idea.

```bash
$ ./target/debug/covidrs Italy Germany USA
+---------------+-------------+------------+-----------+
| Country       | Total Cases | Deaths     | New Cases |
+---------------+-------------+------------+-----------+
| Italy         |      128948 |      15887 |      4316 |
| Germany       |      100123 |       1584 |      4031 |
| USA           |      334730 |       9572 |     23373 |
+---------------+-------------+------------+-----------+
```

# Design

The design of the application tries to follow the Clean Architecture principles.
At this moment the application is broken down into several modules, each
corresponsing to the relevant items on the diagram below:

![Clean Architecture circles](https://blog.cleancoder.com/uncle-bob/images/2012-08-13-the-clean-architecture/CleanArchitecture.jpg)

(image credits: https://blog.cleancoder.com/uncle-bob)

Modules:
 * entity
 * usecase
 * presenter
 * controller
 * view

## Entity

Contains just the definitions of the data structures used across the aplication.

## Usecase

The usecase is designed around the `LatestCountryData` structure and
furthermore, it include two traits `LatestCountryDataInPort` and
`LatestCountryDataOutPort`. These are the input and output, respectively, to and
from the use case layer.

The `LatestCountryData` implements the first trait (direction in) and is using
the second one (direction out). This is neat trick to allow it to get the
request and pass along the response without exposig too much to the neighbouring
layers and, which is the most important, make sure that the dependencies chain
is directed inwards (looking at the diagram above).

## Controller

Controller is a liason between the "user input" and reacting to it (use case).
In this application there is no real user input because there is no interactive
GUI however starting it up with parameters is, sort of, an input.

So the Controller takes the commandline parameters and turns them into the
`LatestDataRequest` structure. It is then passed along to the use case layer.

## Presenter

The presenter sits between the concrete view (think cli or gui or whatever else
there might be) and its sole respnsibility is to get the output from the use
case layer and convert it to the "viewable" format.

By "viewable format" I mean a simple data structure that can be displayed by the
view with minimal processing required. In this case the `ViewModel` is just a 
collection of lines (`Vec` of `String`) however in more complex cases there could
be `booleans` to indicate which parts of the UI should be active and which
should not.

The presenter layer is following the pattern used for the use case later and 
thus it defines the `PresenterOutPort` trait for getting the data out of it. It
also implements the use cases's `LatestCountryDataOutPort` so that it can get
the data from the use case layer - remmeber the use case is using this trait as
a transport to wherever the resuls should go.

There is only one presenter defined at the moment and it is called `Table` for
what it is doing - converting the results into a simple cli table. If, for
example, there would be a need for the JSON output it would be implemented right
next to the `Table` presenter.

## View

The view module is responsible for being the "display" for the application. It
defines a single structure called `Cli`. It implements the presenter's
`PresenterOutPort` to get the `ViewModel` out of the presenter.

There is no processing of the `ViewModel` at all. The view simply and blindly
iterates over the elements of the view model and prints them on the screen "as
is". 

# Shortcuts

There have been a few shortcuts taken.

First, there is no Repository implementation at all and the use case is directly
grabbing the data. Ideally, it should not be done this way and in the best of
the possible worlds the use case would use the "repository" for the purpose of
acquiring the current Covid-19 state.

Second, all of the data structures are defined in the `entity` module but the
`ViewModel` should not be there. The definition of this structure should
exist somewhere between the View and the Presenter as it is not quite a part of
the domain of the application.

Finally, the concrete structures such as `View`, `Cli`, `Controller`, etc... all
should be the implementations of the relevant traits. 

Lastly, I do not think that it is correct to have the presentation of the
`LatestDataResponse` structure implemented along its definition (the impl of the
`Display` trait). The more I think of it the more I am certain that it should be
coded in the presentaton layer - for all teh reasons I can think of the entity
should not know how the final presentation would look like.
